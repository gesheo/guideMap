
const Index =resolve => require(['@/views/Index'], resolve);
// const SearchResult =resolve => require(['@/views/SearchResult'], resolve);
// const MemberDetails =resolve => require(['@/views/MemberDetails'], resolve);
// const UserCenter =resolve => require(['@/views/UserCenter'], resolve);
// const EditPersonalInfo =resolve => require(['@/views/EditPersonalInfo'], resolve);

const routers =[{
    path:'/index',
    name:'index',
    component:Index,
    meta:{index: 0}
}/*, { 
    path:'/searchresult',
    name:'searchresult',
    component:SearchResult,
    meta:{index: 1}
}, { 
    path:'/index/members',
    name:'members',
    component:MemberDetails,
    meta:{index: 2}
}, { 
    path:'/usercenter',
    name:'usercenter',
    component:UserCenter,
    meta:{index: 1}
}, { 
    path:'/usercenter/editpersonalinfo',
    name:'editinfo',
    component:EditPersonalInfo,
    meta:{index: 2}
}*/, { 
    path:'/',
    redirect:'/index',
    meta:{index: 0}
}]

export default routers;