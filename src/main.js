// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';

// // axios 引入
import axios from 'axios'
import VueAxios from 'vue-axios'
import qs from 'qs'

Vue.use(VueAxios, axios)
Vue.prototype.qs = qs
axios.defaults.baseURL='https://guide-api.cnqcnet.com'

// 引入vant ui
import { Popup ,Slider ,Dialog  } from 'vant'
Vue.use(Popup).use(Slider).use(Dialog )
// Vue
//引入Leaflet对象样式文件
import 'leaflet/dist/leaflet.css'
//引入Leaflet对象 挂载到Vue上，便于全局使用，也可以单独页面中单独引用
import L from 'leaflet' 
import markercluster from 'leaflet.markercluster' 
Vue.L = Vue.prototype.$L = L 

// 路由引入
import VueRouter from 'vue-router'
import routes from './routers.js'

Vue.use(VueRouter)

// 公用js
import './common/js/common.js'

//css重置 
import '@/common/css/reset.css'
import '@/common/css/vant_style.scss'

Vue.config.productionTip = false

const router =new VueRouter({
    mode: 'history',
    base: __dirname,
    routes
});


/* eslint-disable no-new */
new Vue({
    router,
    el: '#app',
    components: { App },
    template: '<App/>'
})
