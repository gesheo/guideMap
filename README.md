# 预览地址：https://guidemap.cnqcnet.com/index?id=5&key=111
![输入图片说明](https://images.gitee.com/uploads/images/2020/0325/115326_9da72f98_474005.png "QQ20200325-115105@2x.png")
# 导览系统

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
